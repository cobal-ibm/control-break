       IDENTIFICATION DIVISION.
       PROGRAM-ID. Control_B3.


       ENVIRONMENT DIVISION. 
       INPUT-OUTPUT SECTION. 
       FILE-CONTROL. 

           SELECT 100-INPUT-FILE ASSIGN TO "DATA3.DAT"
           ORGANIZATION IS LINE SEQUENTIAL
           FILE STATUS IS WS-INPUT-FILE-STATUS.

           SELECT 200-OUTPUT-FILE ASSIGN TO "REPORT3.RPT"
           ORGANIZATION IS LINE SEQUENTIAL
           FILE STATUS IS WS-OUTPUT-FILE-STATUS.

       DATA DIVISION. 
       FILE SECTION. 
       FD  100-INPUT-FILE
           BLOCK CONTAINS 0 RECORDS. 

       01  INPUT-FILE-RECORD.
           05 BRANCH-ID                PIC X.
           05 FILLER                   PIC X(6) VALUE SPACE.
           05 DATE-DMY.
              10 DATE-DAY              PIC X(2).
              10 FILLER                PIC X.
              10 DATE-MONTH            PIC X(2).
              10 FILLER                PIC X.
              10 DATE-YEAR             PIC X(2).

           05 FILLER                   PIC X(3) VALUE SPACE.
           05 PRODUCT                  PIC X(9).
           05 INCOME                   PIC 9(3).

       
       FD  200-OUTPUT-FILE
           BLOCK CONTAINS 0 RECORDS.

       01  OUTPUT-FILE-RECORD         PIC X(80). 

       WORKING-STORAGE SECTION. 
       
       01  WS-CAL.
           05 WS-INPUT-FILE-COUNT     PIC 9(5).
           05 WS-ALL-TOTAL            PIC 9(5).
           05 WS-BRANCH-CB            PIC X.
           05 WS-BRANCH-SHOW          PIC X VALUE "S".
           05 WS-BRANCH-TOTAL         PIC 9(5).
           05 WS-DATE-TOTAL           PIC 9(5).
           05 WS-DATE-CB              PIC X(8).
           05 WS-DATE-SHOW            PIC X VALUE "S".
           05 WS-PRO-TOTAL            PIC 9(5).
           05 WS-PRO-CB               PIC X(9).
       
       01  WS-FORMAT.
           05 WS-YMD-FORMAT.
              10 WS-YEAR             PIC X(2).
              10 FILLER              PIC X VALUE "-".
              10 WS-MONTH            PIC X(2).
              10 FILLER              PIC X VALUE "-".
              10 WS-DAY              PIC X(2).             


       01  WS-INPUT-FILE-STATUS       PIC X(2).
           88 FILE-OK                 VALUE "00".
           88 FILE-AT-END             VALUE "10".

       01  WS-OUTPUT-FILE-STATUS      PIC X(2).
           88 FILE-OK                 VALUE "00".
           88 FILE-AT-END             VALUE "10".
         
       01  RPT-FORMAT.

           05 RPT-HEADER              PIC X(37) 
                    VALUE "BRANCH   DATE   PRODUCT        INCOME".
           
           05 RPT-DETAIL.
              10 RPT-BRANCH           PIC X.
              10 FILLER               PIC X(6) VALUE SPACE.
              10 RPT-DATE             PIC X(8).
              10 FILLER               PIC X(2) VALUE SPACE.
              10 RPT-PRO              PIC X(9).
              10 FILLER               PIC X(6) VALUE SPACE.
              10 RPT-TOTAL            PIC ZZZZ9.

           05 RPT-DATE-TOTAL.
              10 FILLER               PIC X(32) 
                   VALUE "            DATE TOTAL         ".
              10 RPT-TOTAL            PIC ZZZZ9.

           05 RPT-BRANCH-TOTAL.
              10 FILLER               PIC X(32) 
                   VALUE "----------BRANCH TOTAL----------".
              10 RPT-TOTAL            PIC ZZZZ9.
           
           05 RPT-ALL-TOTAL.
              10 FILLER               PIC X(32) 
                   VALUE ">          ALL TOTAL           <".
              10 RPT-TOTAL            PIC ZZZZ9.
              



           


       PROCEDURE DIVISION.
       0000-MAIN.
           PERFORM 1000-INITIAL THRU 1000-EXIT
           PERFORM 2000-PROCESS THRU 2000-EXIT 
              UNTIL FILE-AT-END OF WS-INPUT-FILE-STATUS 
           PERFORM 3000-END     THRU 3000-EXIT

           GOBACK 
           .



       1000-INITIAL.
           PERFORM 1100-OPEN-INPUT THRU 1100-EXIT
           PERFORM 1200-OPEN-OUTPUT THRU 1200-EXIT
           MOVE ZERO TO WS-ALL-TOTAL 
           MOVE RPT-HEADER TO OUTPUT-FILE-RECORD 
           PERFORM 7000-WRITE THRU 7000-EXIT 
           PERFORM 8000-READ THRU 8000-EXIT
           .


       1000-EXIT.
           EXIT
           .
       


       1100-OPEN-INPUT.
           OPEN INPUT 100-INPUT-FILE
           IF FILE-OK OF WS-INPUT-FILE-STATUS 
              CONTINUE
           ELSE
              DISPLAY "******  CONBREAK3 ABEND *****"
              UPON CONSOLE
              DISPLAY "* PARA 1100-OPEN-INPUT *"
              UPON CONSOLE 
              DISPLAY "* FILE STATUS : " WS-INPUT-FILE-STATUS  " *"
              UPON CONSOLE 
              DISPLAY "******  CONBREAK3 ABEND *****"
              UPON CONSOLE  
              STOP RUN
           END-IF 
           .

       1100-EXIT.
           EXIT.
       
       1200-OPEN-OUTPUT.
           OPEN OUTPUT 200-OUTPUT-FILE
           IF FILE-OK OF WS-OUTPUT-FILE-STATUS 
              CONTINUE
           ELSE
              DISPLAY "******  CONBREAK3 ABEND *****"
              UPON CONSOLE
              DISPLAY "* PARA 1200-OPEN-OUTPUT *"
              UPON CONSOLE 
              DISPLAY "* FILE STATUS : " WS-OUTPUT-FILE-STATUS  " *"
              UPON CONSOLE 
              DISPLAY "******  CONBREAK3 ABEND *****"
              UPON CONSOLE  
              STOP RUN
           END-IF 
           .

       1200-EXIT.
           EXIT.


       2000-PROCESS.
           
           MOVE BRANCH-ID TO WS-BRANCH-CB 
           MOVE ZERO TO WS-BRANCH-TOTAL 

           PERFORM 2100-PROCESS-BRANCH THRU 2100-EXIT
              UNTIL FILE-AT-END OF WS-INPUT-FILE-STATUS 
                 OR BRANCH-ID NOT = WS-BRANCH-CB
           
           MOVE WS-BRANCH-TOTAL TO RPT-TOTAL OF RPT-BRANCH-TOTAL 
           MOVE RPT-BRANCH-TOTAL TO OUTPUT-FILE-RECORD 
           PERFORM 7000-WRITE THRU 7000-EXIT 
           MOVE "S" TO WS-BRANCH-SHOW 
      *    DISPLAY BRANCH-ID  " " WS-BRANCH-CB " " WS-BRANCH-TOTAL 


           .
       2000-EXIT.
           EXIT.
       
       2100-PROCESS-BRANCH.

           MOVE DATE-DMY TO WS-DATE-CB 
           MOVE ZERO TO WS-DATE-TOTAL 

           PERFORM 2200-PROCESS-DATE THRU 2200-EXIT
              UNTIL FILE-AT-END OF WS-INPUT-FILE-STATUS 
                 OR BRANCH-ID NOT = WS-BRANCH-CB
                 OR DATE-DMY  NOT = WS-DATE-CB
           
           MOVE WS-DATE-TOTAL TO RPT-TOTAL OF RPT-DATE-TOTAL 
           MOVE RPT-DATE-TOTAL TO OUTPUT-FILE-RECORD 
           PERFORM 7000-WRITE THRU 7000-EXIT 
           MOVE "S" TO WS-DATE-SHOW 
      *    DISPLAY DATE-DMY  " " WS-DATE-CB " " WS-DATE-TOTAL 

           .


       2100-EXIT.
           EXIT.


       2200-PROCESS-DATE.

           MOVE PRODUCT TO WS-PRO-CB 
           MOVE ZERO TO WS-PRO-TOTAL 

           PERFORM 2300-PROCESS-PRODUCT THRU 2300-EXIT
              UNTIL FILE-AT-END OF WS-INPUT-FILE-STATUS 
                 OR BRANCH-ID NOT = WS-BRANCH-CB
                 OR DATE-DMY  NOT = WS-DATE-CB
                 OR PRODUCT   NOT = WS-PRO-CB
           IF WS-BRANCH-SHOW = "S"
              MOVE WS-BRANCH-CB TO RPT-BRANCH OF RPT-DETAIL 
              MOVE "H" TO WS-BRANCH-SHOW
           ELSE
              MOVE SPACE TO  RPT-BRANCH OF RPT-DETAIL 
           END-IF 

           IF WS-DATE-SHOW = "S"
              MOVE DATE-DAY TO WS-DAY OF WS-YMD-FORMAT 
              MOVE DATE-MONTH TO WS-MONTH OF WS-YMD-FORMAT 
              MOVE DATE-YEAR TO WS-YEAR  OF WS-YMD-FORMAT  
              MOVE WS-YMD-FORMAT TO RPT-DATE OF RPT-DETAIL 
              MOVE "H" TO WS-DATE-SHOW
           ELSE
              MOVE SPACE TO  RPT-DATE OF RPT-DETAIL 
           END-IF 
           
           MOVE WS-PRO-CB    TO RPT-PRO    OF RPT-DETAIL 
           MOVE WS-PRO-TOTAL TO RPT-TOTAL OF RPT-DETAIL
           MOVE RPT-DETAIL TO OUTPUT-FILE-RECORD 
           PERFORM 7000-WRITE THRU 7000-EXIT 
      *    DISPLAY PRODUCT  " " WS-PRO-CB " " WS-PRO-TOTAL  
           
           .


       2200-EXIT.
           EXIT.
       

       2300-PROCESS-PRODUCT.

      *    DISPLAY BRANCH-ID " " DATE-DMY  " " PRODUCT " " INCOME
           ADD INCOME TO WS-ALL-TOTAL 
           ADD INCOME TO WS-BRANCH-TOTAL 
           ADD INCOME TO WS-DATE-TOTAL 
           ADD INCOME TO WS-PRO-TOTAL 

           
           PERFORM 8000-READ THRU 8000-EXIT
           .


       2300-EXIT.
           EXIT.

       3000-END.
      *    DISPLAY "All Total : " WS-ALL-TOTAL 
           MOVE WS-ALL-TOTAL TO RPT-TOTAL OF RPT-ALL-TOTAL 
           MOVE RPT-ALL-TOTAL TO OUTPUT-FILE-RECORD 
           PERFORM 7000-WRITE THRU 7000-EXIT  
           CLOSE 100-INPUT-FILE 200-OUTPUT-FILE 
           

           DISPLAY "Read Input file : " WS-INPUT-FILE-COUNT 
           .
       

       3000-EXIT.
           EXIT.

       
       7000-WRITE.
           WRITE OUTPUT-FILE-RECORD 
           IF FILE-OK OF WS-OUTPUT-FILE-STATUS 
              CONTINUE
           ELSE
              DISPLAY "******  CONBREAK3 ABEND *****"
              UPON CONSOLE
              DISPLAY "* PARA 7000-WRITE *"
              UPON CONSOLE 
              DISPLAY "* FILE STATUS : " WS-OUTPUT-FILE-STATUS  " *"
              UPON CONSOLE 
              DISPLAY "******  CONBREAK3 ABEND *****"
              UPON CONSOLE  
              STOP RUN
           END-IF 
           .
       
       7000-EXIT.
           EXIT.

       
       8000-READ.
           READ 100-INPUT-FILE
           IF FILE-OK OF WS-INPUT-FILE-STATUS 
              ADD 1 TO WS-INPUT-FILE-COUNT 
           ELSE
              IF FILE-AT-END OF WS-INPUT-FILE-STATUS 
                 CONTINUE
              ELSE
                 DISPLAY "******  CONBREAK3 ABEND *****"
                 UPON CONSOLE
                 DISPLAY "* PARA 8000-READ *"
                 UPON CONSOLE 
                 DISPLAY "* FILE STATUS : " WS-INPUT-FILE-STATUS  " *"
                 UPON CONSOLE 
                 DISPLAY "******  CONBREAK3 ABEND *****"
                 UPON CONSOLE
                 STOP RUN
             END-IF   
           END-IF 
           .


       8000-EXIT.
           EXIT.
